

# Explorational weather bot

This weather bot is an entry point in developing conversational chat bots. This weather bot is designed to communicate with Rocket.Chat, but should be easily applicable to other platforms.

## How to run this repository
```text
docker build -t foo . && docker run -it foo
```

## Rasa stack structure (german)
**Struktur Rasa NLU**:

* Trainingsdatei (genaues Format: https://nlu.rasa.com/dataformat.html -> .json-Datei)
	Mögliche eingehende Nachrichten
	Entitäten, welche in a. auftreten
	Intentionen, zugehörig zu a.

* Konfigurationsdatei: Einstellungen zum Ausführen des Trainingprozesses (z.B. verwendetes ML-Framework; mehr: https://nlu.rasa.com/config.html)

**Struktur Rasa Core**:
  * Interpreter (NLU): Parsen von Text zu Entitäten und Intentionen
  
  * Domain (Universum des Bots, wird in domain.yml gespeichert):
    - Intentionen (NLU)
	- Entitäten (NLU)
	- Templates: Nachrichten welche der Bot später zurücksenden 		      kann, werden mit utter_[intent] definiert
	- Actions: Liste von Dingen die der Bot machen/sagen kann, entweder Template oder eigen Nachricht (z.B. wenn API-Call erforderlich -> python-Skript)
	- Slots (Nutzer-spezifische Variablen, welche im Laufe der Konversation wichtig werden)
* Stories: Kommunikationsfluss (-> nächst auszuführende Aktion wird durch Wahrscheinlichkeitsmodell ausgeführt; stories.md-Datei)
  - Supervised Learning: Eigenes Erstellen der Datei
  - Reinforcement Learning: Nach jeder Nachricht an Bot wird nachgefragt, ob der Bot alles richtig verstanden hat

[Reference](https://velotio.com/blog/2018/2/15/stateless-bot-using-rasa-stack)

## Outlook
* [Multilingualer Bot](https://github.com/nmstoker/MultiLingualBot) - Mehrsprachigkeit eines Bots
* [Ausfallsicherung von Bots](https://blog.spg.ai/failing-gracefully-with-rasa-nlu-14a7d8e53af9) - Wie reagiert der Bot auf Fragen über seinen Scope hinaus?
* [Trainingsdaten für den Bot + Kontinuierliche Verbesserung](https://blog.ubisend.com/optimise-chatbots/chatbot-training-data)
* [Analytic-Dashboard](https://github.com/Azure/ibex-dashboard)
* GUIs zur Eingabe von Trainingsdaten:
	- [Rasa lokal](https://github.com/RasaHQ/rasa-nlu-trainer)
	- [Rasa online](https://rasahq.github.io/rasa-nlu-trainer/)
	- [Articulate](https://github.com/samtecspg/articulate)
* [Adaptive Cards](https://adaptivecards.io/) - Ermöglicht schöneres Dialogdesign
* [Multi-Intent Erkennung](https://medium.com/rasa-blog/how-to-handle-multiple-intents-per-input-using-rasa-nlu-tensorflow-pipeline-75698b49c383) - Zu Erkennung mehrerer Intention innerhalb einer Nachricht

## Other useful resources:
* [Rasa NLU installation](https://nlu.rasa.com/installation.html) - guidelines on how to install Rasa NLU.
* [Rasa Core installation](https://core.rasa.com/installation.html) - guidelines on how to install Rasa Core.
